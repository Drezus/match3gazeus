﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ObjectPool
{
    public string Name;

    ///Pools may have different styles of prefabs.
    public GameObject[] prefabs;
    public int poolSize;
    public Transform poolParent;

    [HideInInspector]
    public List<GameObject> pool;
}
