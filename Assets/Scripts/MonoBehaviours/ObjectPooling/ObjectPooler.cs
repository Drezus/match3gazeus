﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObjectPooler : Singleton<ObjectPooler>
{
    [Header("Managed Pools")]
    public ObjectPool[] Pools;

    public void Awake()
    {
        foreach (ObjectPool pool in Pools)
        {
            InitializePool(pool);
        }
    }

    private void InitializePool(ObjectPool objPool)
    {
        objPool.pool = new List<GameObject>();

        for (int i = 0; i < objPool.poolSize; i++)
        {
            GameObject obj = Instantiate(objPool.prefabs[Random.Range(0, objPool.prefabs.Length)], objPool.poolParent);
            obj.SetActive(false);
            objPool.pool.Add(obj);
        }
    }

    public GameObject SpawnPooledObject(int poolID, Vector2 pos, Quaternion rot)
    {
        GameObject gameObj = Pools[poolID].pool.FirstOrDefault(x => x.activeSelf == false);

        if (gameObj == null)
        {
            //Oops! Pool was too short to handle the demand. Let's spawn some more.
            gameObj = Instantiate(Pools[poolID].prefabs[Random.Range(0, Pools[poolID].prefabs.Length)], Pools[poolID].poolParent);
            Pools[poolID].poolSize++;
            Debug.LogWarning("Adding extra " + gameObj.name + " gameobjects to the " + Pools[poolID].Name + " pool to supply the demand. " +
                "Expected Size: " + Pools[poolID].poolSize.ToString());
        }

        gameObj.SetActive(true);
        gameObj.transform.position = pos;
        gameObj.transform.rotation = rot;

        Pools[poolID].pool.Add(gameObj);

        return gameObj;
    }

    public void RecyclePooledGameObject(GameObject gameObj, int poolID)
    {
        gameObj.transform.SetParent(Pools[poolID].poolParent);
        gameObj.SetActive(false);
    }
}
