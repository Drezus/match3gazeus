﻿using System;
using UnityEngine;

public class Piece : MonoBehaviour
{
    public SpriteRenderer rend;
    public Rigidbody2D rbody;
    public Vector2Int gridCoords;

    [HideInInspector]
    public Vector2Int? swapTarget;

    [HideInInspector]
    public PieceShapes shape;
    [HideInInspector]
    public bool falling;
    [HideInInspector]
    public bool swapFinished;

    #region Falling-related variables
    private Vector2 originalPos;
    private Piece belowPiece => BoardManager.Instance.grid[gridCoords.x, Mathf.Max(gridCoords.y - 1, 0)];
    public bool gapBelow => belowPiece == null || belowPiece.falling;
    #endregion

    public void Initialize(PieceType type, Vector2Int coord)
    {
        shape = type.shape;
        rend.sprite = type.icon;
        rend.color = type.color;
        gridCoords = coord;

        falling = false;
        swapFinished = false;
        swapTarget = null;

        SnapToGrid();

        //Forces the newly-created piece to stop falling right away if it
        //spawns on the topmost row with another piece stopped below it.
        if (gridCoords.y == BoardManager.Instance.boardSize.y - 1)
        {
            Piece secondTopmostPiece = BoardManager.Instance.grid[gridCoords.x, BoardManager.Instance.boardSize.y - 2];
            if (secondTopmostPiece != null) StopFalling();
        }
    }

    public void SnapToGrid()
    {
        transform.position = BoardManager.Instance.PositionFromTile(gridCoords);
        originalPos = transform.position;
    }

    public void OnMouseDown()
    {
        if (!InputManager.Instance.canMove || falling) return; //Pieces can't be moved if they're falling.

        InputManager.Instance.currentPiece = this;
    }

    private void LateUpdate()
    {
        /// Originally, this function only executed when pieces were deleted (the deleted pieces would turn on the falling state of the pieces above.
        /// But this led to many bugs with pieces sinking into other pieces or creating gaps between pieces.
        /// Hence, I decided it'd be more practical to leave pieces to detect if they should fall or not by themselves.
        if (falling)
        {
            Vector2 gridPos = BoardManager.Instance.PositionFromTile(gridCoords);

            if (transform.position.y < gridPos.y)
            {
                if (!gapBelow)
                {
                    StopFalling();
                }
                else
                {
                    StartFalling();
                }
            }
        }
        else
        {
            if (gapBelow && !falling)
            {
                StartFalling();
            }
        }

        //When a position is assigned to Vector2 targetPos, it means the piece is meant to be moving towards a given input direction.
        if (swapTarget.HasValue)
        {
            if (Vector2.Distance(BoardManager.Instance.PositionFromTile(swapTarget.Value), transform.position) >= 0.1f)
            {
                //Keep updating the movement.
                transform.position = Vector2.Lerp(transform.position, BoardManager.Instance.PositionFromTile(swapTarget.Value), .3f);
            }
            else
            {
                //End movement. Snap to grid and signal BoardManager.
                gridCoords = swapTarget.Value;
                BoardManager.Instance.grid[gridCoords.x, gridCoords.y] = this;
                SnapToGrid();
                swapFinished = true;
                swapTarget = null;
            }
        }
    }

    public void StartFalling()
    {
        //Never falls if its on the floor.
        if (gridCoords.y <= 0)
        {
            if (falling) StopFalling();
            return;
        }

        falling = true;
        rbody.bodyType = RigidbodyType2D.Dynamic;

        //Already sets this pieces's grid coordinate as the below's slot even before reaching it, 
        //since it won't be considered to matches in this position anymore.

        //To avoid leaving a trail of their own reference behind, we clear the grid's current spot before the change.
        BoardManager.Instance.grid[gridCoords.x, gridCoords.y] = null;

        gridCoords = new Vector2Int(gridCoords.x, gridCoords.y - 1);
        BoardManager.Instance.grid[gridCoords.x, gridCoords.y] = this;
    }

    private void StopFalling()
    {
        falling = false;
        rbody.bodyType = RigidbodyType2D.Static;
        SnapToGrid();

        //Attempt matches as it lands on a new spot.
        BoardManager.Instance.AttemptMatch(this);
    }
}
