﻿using System;
using UnityEngine;

[Serializable]
public struct PieceType
{
    public PieceShapes shape;
    public Sprite icon;
    public Color color;
}
