﻿using System.Collections.Generic;
using UnityEngine;

public struct MatchList
{
    public List<Piece> horizontalMatches;
    public List<Piece> verticalMatches;

    public bool hasHorizontalMatches
    {
        get
        {
            if (horizontalMatches == null)
            {
                return false;
            }
            else
            {
                return horizontalMatches.Count >= BoardManager.Instance.minMatch;
            }
        }
    }

    public bool hasVerticalMatches
    {
        get
        {
            if (verticalMatches == null)
            {
                return false;
            }
            else
            {
                return verticalMatches.Count >= BoardManager.Instance.minMatch;
            }
        }
    }

    public bool hasMatches { get { return (hasHorizontalMatches || hasVerticalMatches); } }
}