﻿public enum PieceShapes
{
    Square,
    Circle,
    Triangle,
    Star,
    Heart
}