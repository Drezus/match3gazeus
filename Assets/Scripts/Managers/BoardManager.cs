﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : Singleton<BoardManager>
{
    [Header("Game Settings")]
    public GameObject piecePrefab;
    public Vector2Int boardSize;
    public Vector2 spacing;
    public int minMatch = 3;
    public PieceType[] pieceTypes;
    [Header("Public References")]
    public Transform pieceParent;

    #region Game variables
    public Piece[,] grid;
    private int maxPieces => boardSize.x * boardSize.y;
    private int spawnedPieces = 0;
    #endregion

    public Vector2 PositionFromTile(Vector2Int tile)
    {
        return new Vector2(tile.x * spacing.x, tile.y * spacing.y);
    }

    void Start()
    {
        //Some failsafes for board generation
        if (boardSize.x < minMatch && boardSize.y < minMatch)
        {
            Debug.LogError("Minimum match size is too big compared to board size!");
            return;
        }
        else if (boardSize.y < 2)
        {
            Debug.LogError("Board is too short for the game to work properly. Please, make it so the board is at least 3 pieces tall.");
            return;
        }
        grid = new Piece[boardSize.x, boardSize.y];
        spawnedPieces = 0;
        FillBoard();
    }

    void FillBoard()
    {
        for (int i = 0; i < boardSize.x; i++)
        {
            for (int j = 0; j < boardSize.y; j++)
            {
                CreateNewPiece(i, j);
            }
        }
    }

    private Piece CreateNewPiece(int x, int y)
    {
        Vector2Int currentTile = new Vector2Int(x, y);
        Vector2 pos = PositionFromTile(currentTile);

        GameObject GO = ObjectPooler.Instance.SpawnPooledObject(0, pos, Quaternion.identity);
        GO.transform.SetParent(pieceParent);
        Piece p = GO.GetComponent<Piece>();

        grid[x, y] = p;

        //Randomizes between all piece types and assigns it to the newly instantiated piece.
        //Also takes into account possible matches. If they were to happen, we just randomize again
        //until the new piece is unmatched with its surroundings.
        //We keep this loop within reason by limiting the attempts amount to five times the amount of different pieces.
        int attempts = 0;
        do
        {
            var randomType = pieceTypes[UnityEngine.Random.Range((int)0, (int)pieceTypes.Length)];
            p.Initialize(randomType, currentTile);
            attempts++;
        }
        while (GetMatchesForPiece(p).hasMatches && attempts <= pieceTypes.Length * 5);

        spawnedPieces++;

        return p;
    }

    private MatchList GetMatchesForPiece(Piece p)
    {
        MatchList matches = new MatchList();
        matches.horizontalMatches = new List<Piece>();
        matches.verticalMatches = new List<Piece>();

        matches.horizontalMatches.Add(p);
        matches.verticalMatches.Add(p);

        //Find horizontal matches
        //To the right
        for (int x = p.gridCoords.x + 1; x < boardSize.x; x++)
        {
            if (x < 0 || x >= boardSize.x) break;

            Piece targetPiece = grid[x, p.gridCoords.y];

            if (targetPiece == null) break;

            if (targetPiece.shape == p.shape)
            {
                matches.horizontalMatches.Add(targetPiece);
            }
            else
            {
                break;
            }
        }

        //To the left
        for (int x = p.gridCoords.x - 1; x >= 0; x--)
        {
            if (x < 0 || x >= boardSize.x) break;

            Piece targetPiece = grid[x, p.gridCoords.y];

            if (targetPiece == null) break;

            if (targetPiece.shape == p.shape)
            {
                matches.horizontalMatches.Add(targetPiece);
            }
            else
            {
                break;
            }
        }


        //Find vertical matches
        //Upwards
        for (int y = p.gridCoords.y + 1; y < boardSize.y; y++)
        {
            if (y < 0 || y >= boardSize.y) break;

            Piece targetPiece = grid[p.gridCoords.x, y];

            if (targetPiece == null) break;

            if (targetPiece.shape == p.shape)
            {
                matches.verticalMatches.Add(targetPiece);
            }
            else
            {
                break;
            }
        }

        //Downwards
        for (int y = p.gridCoords.y - 1; y >= 0; y--)
        {
            if (y < 0 || y >= boardSize.y) break;

            Piece targetPiece = grid[p.gridCoords.x, y];

            if (targetPiece == null) break;

            if (targetPiece.shape == p.shape)
            {
                matches.verticalMatches.Add(targetPiece);
            }
            else
            {
                break;
            }
        }

        return matches;
    }

    private void LateUpdate()
    {
        SpawnPiecesInVacantSpots();
    }

    /// <summary>
    /// Previously, this function was used on-demand only when a top piece fell from the topmost slot, or when a match
    /// with a topmost piece was made (thus clearing the topmost piece), but this led to many piece duplicating bugs.
    /// I've found more pratical and easier to mantain to let BoardManager decide by itself when it should spawn new pieces
    /// to refill the board.
    /// </summary>
    private void SpawnPiecesInVacantSpots()
    {
        for (int x = 0; x < boardSize.x; x++)
        {
            Piece topMostPiece = grid[x, boardSize.y - 1];
            Piece secondTopmostPiece = grid[x, boardSize.y - 2];

            if (topMostPiece == null &&
                (secondTopmostPiece == null || secondTopmostPiece != null && !secondTopmostPiece.falling))
            {
                SpawnPiece(x);
            }
        }
    }

    public void AttemptSwap(Vector2Int coordA, Vector2Int coordB)
    {
        //Cuts process short if its out-of-bounds.
        if ((coordA.x < 0 || coordA.x >= boardSize.x || coordA.y < 0 || coordA.y >= boardSize.y) ||
           (coordB.x < 0 || coordB.x >= boardSize.x || coordB.y < 0 || coordB.y >= boardSize.y)) return;

        Piece pieceA = grid[coordA.x, coordA.y];
        Piece pieceB = grid[coordB.x, coordB.y];

        //Cuts process short if one of the pieces somehow doesn't exist (may happen when there's a gap between falling pieces)
        if (pieceA == null || pieceB == null) return;

        StartCoroutine(PieceSwapAnimation(coordA, coordB, pieceA, pieceB));
    }

    private IEnumerator PieceSwapAnimation(Vector2Int coordA, Vector2Int coordB, Piece pieceA, Piece pieceB)
    {
        //Do not allow the player to select any other piece while these ones are moving.
        InputManager.Instance.LockInput();

        pieceA.swapTarget = coordB;
        pieceB.swapTarget = coordA;

        yield return new WaitUntil(() => pieceA.swapFinished && pieceB.swapFinished);
        yield return new WaitForSeconds(InputManager.Instance.pieceSwapDelay); //This makes swaps look better and less crude.
        pieceA.swapFinished = false;
        pieceB.swapFinished = false;

        bool aMatches = AttemptMatch(pieceA);
        bool bMatches = AttemptMatch(pieceB);

        //If neither piece matches, return them to their original location.
        if (!aMatches && !bMatches)
        {
            pieceA.swapTarget = coordA;
            pieceB.swapTarget = coordB;

            yield return new WaitUntil(() => pieceA.swapFinished && pieceB.swapFinished);
            yield return new WaitForSeconds(InputManager.Instance.pieceSwapDelay); //This makes swaps look better and less crude.
            pieceA.swapFinished = false;
            pieceB.swapFinished = false;
        }

        InputManager.Instance.ReleaseInput();
    }

    public bool AttemptMatch(Piece p)
    {
        MatchList matches = GetMatchesForPiece(p);

        if (matches.hasMatches)
        {
            List<Piece> piecesToDestroy = new List<Piece>();
            piecesToDestroy.Add(p);

            if (matches.hasHorizontalMatches) piecesToDestroy.AddRange(matches.horizontalMatches);
            if (matches.hasVerticalMatches) piecesToDestroy.AddRange(matches.verticalMatches);

            DestroyPieces(piecesToDestroy);

            return true;
        }
        else
        {
            return false;
        }
    }

    private void DestroyPieces(List<Piece> pieces)
    {
        foreach (Piece p in pieces)
        {
            //Remove this piece from the grid
            grid[p.gridCoords.x, p.gridCoords.y] = null;
            spawnedPieces--;

            //Recycle via Object Pooling
            ObjectPooler.Instance.RecyclePooledGameObject(p.gameObject, 0);
        }
    }

    public void SpawnPiece(int x)
    {
        //Rejects a new spawn if the board is already filled up.
        if (spawnedPieces >= maxPieces) return;

        Piece newP = CreateNewPiece(x, boardSize.y - 1);
    }
}