﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : Singleton<InputManager>
{
    [HideInInspector]
    public Piece currentPiece = null;
    [HideInInspector]
    public bool canMove = true;

    public float pieceSwapDelay = 0.2f;

    private void Update()
    {
        if (!Input.GetMouseButton(0)) currentPiece = null;

        if (currentPiece == null) return;

        var mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        var dragDistance = Vector2.Distance(currentPiece.transform.position, mouseWorldPos);

        if (dragDistance >= BoardManager.Instance.spacing.x / 1.85f)
        {
            //Finds out which direction the piece will move.
            if (Mathf.Abs(currentPiece.transform.position.x - mouseWorldPos.x) > Mathf.Abs(currentPiece.transform.position.y - mouseWorldPos.y))
            {
                //Moves horizontally
                if (mouseWorldPos.x > currentPiece.transform.position.x)
                {
                    //Moves right
                    BoardManager.Instance.AttemptSwap(currentPiece.gridCoords, new Vector2Int(currentPiece.gridCoords.x + 1, currentPiece.gridCoords.y));
                }
                else
                {
                    //Moves left
                    BoardManager.Instance.AttemptSwap(currentPiece.gridCoords, new Vector2Int(currentPiece.gridCoords.x - 1, currentPiece.gridCoords.y));
                }
            }
            else
            {
                //Moves vertically
                if (mouseWorldPos.y > currentPiece.transform.position.y)
                {
                    //Moves up
                    BoardManager.Instance.AttemptSwap(currentPiece.gridCoords, new Vector2Int(currentPiece.gridCoords.x, currentPiece.gridCoords.y + 1));
                }
                else
                {
                    //Moves down
                    BoardManager.Instance.AttemptSwap(currentPiece.gridCoords, new Vector2Int(currentPiece.gridCoords.x, currentPiece.gridCoords.y - 1));
                }
            }
        }
    }

    public void LockInput()
    {
        canMove = false;
        currentPiece = null;
    }

    public void ReleaseInput()
    {
        canMove = true;
    }
}
